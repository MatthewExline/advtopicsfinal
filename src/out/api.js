YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "Ajax",
        "CartGraph",
        "CartList",
        "CartUpdater"
    ],
    "modules": [
        "exline"
    ],
    "allModules": [
        {
            "displayName": "exline",
            "name": "exline"
        }
    ],
    "elements": []
} };
});