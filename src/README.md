This project is a virtual 'cart' manager that interacts with a database.  Rows in the database are depicted as "Cart" objects.  The columns in the database contain information about the cart, including how many of what "items" are in the cart.
Rows from the database are displayed as 'cart' image sprites on a GUI div that the user can interact with.  The div can even be mouse-dragged so that the user can position it anywhere on the page that he sees fit.
Clicking on one of the Cart images brings up a Cart Graph, which is a seperate GUI div that displays column-info about that row(Cart). The column-info is meant to depict items being in the cart, each item having a name and an associated dollar-value.  
The user can press up and down arrows on the GUI to increase or decrease the amounts of each item in the Cart.
Each Cart has a "Wallet" which limits the total combined value of items that can be placed in the cart. For example, if the Wallet of a Cart is $100, the user cannot add more than $100 worth of items to the cart.
The user can change the Wallet amount of the Cart at any given time.
The GUI dynamically updates the database as the user interacts with it.