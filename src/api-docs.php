<!DOCTYPE html>
<html>
<head>
	<title>Book Service API Docs</title>
</head>
<body>

<h1>Official API Docs for the Books Web Service</h1>
  <table border="1">
    <tr>
      <th>API CALL (ROUTE)</th>
      <th>METHOD</th>
      <th>ACTION</th>
    </tr>
    <tr>
      <td>books/</td>
      <td>GET</td>
      <td>Get's all books</td>
    </tr>
    <tr>
      <td>books/</td>
      <td>POST</td>
      <td>
        Inserts a new book into the data base <br>
        Expects the Content-Type to be application/json
      </td>
    </tr>
    <tr>
      <td>books/?order_by=x</td>
      <td>GET</td>
      <td>
        Get's all books, ordered by a property of a book <br>
        Note: x could be 'author' or 'title'
      </td>
    </tr>
    <tr>
      <td>books/x</td>
      <td>GET</td>
      <td>
        Get's a book by it's id property <br>
        ex: <b>books/1</b> would fetch the book with an id of 1
      </td>
    </tr>
    <tr>
      <td>books/x</td>
      <td>PUT</td>
      <td>
        Edits the book with id of x <br>
        Note: then Content-Type should be application/json
      </td>
    </tr>
  </table>
  ***By default, responses will return data in JSON format <br>
  ***BUT IF THE 'Accept' request header is set to 'application/xml' THEN WE RETURN THE DATA IN XML FORMAT

</body>
</html>