/**
* @module exline
* 
*/
var exline = exline || {};
/**
* This class builds and handles cart graph window.
*
*@class CartGraph
*/
exline.cartGraph={
	/**
	*Creates a custom notification on the Cart Graph div.
	*
	*@method note
	*@param String str
	*/
	note:function(str){
		if(document.getElementById("notary"))
			document.getElementById("notary").innerHTML=str;
	},
	/**
	*Closes the Cart Graph div.
	*
	*@method closeCartGraph
	*@param none
	*/
	closeCartGraph:function(){
		var div = document.getElementById("cartGraphDiv");
		div.parentNode.removeChild(div);
	},
	/**
	*Displays updated data in the Cart Graph div. Can also call methods to close the Cart Graph and alert notifications in the Cart Graph.
	*
	*@method refreshCart
	*@param {arry} cartDataArray An array that holds data which is used to update the Cart Graph Div. 
	*@param {String} option An optional parameter that can indicate that the Cart Graph div should be closed or give a custom alert.
	*/
	refreshCart:function(cartDataArray, option=""){
		//alert("response: "+cartDataArray);
		var cart=JSON.parse(cartDataArray);
		document.getElementById("cartGraphDiv").setGraphBarsAndInputs(cart);
		exline.CartList.getAllCarts();
		if(option=='close')exline.cartGraph.closeCartGraph();
		else if(option!=""){
			exline.cartGraph.note(option);
		}
	},
	/**
	*Calls the method to build the Cart Graph Div if it doesn't already exist. Then displays cart data in the Cart Graph div.
	*
	*@method graphThisCart
	*@param {array} cartDataArray The data which is used to build the UI of the Cart Graph which represents the Cart.
	*/
	graphThisCart:function(cartDataArray){
		var graphDiv;
		if(document.getElementById("cartGraphDiv")==null)
			exline.cartGraph.buildGraphDiv();
		graphDiv=document.getElementById("cartGraphDiv");
		graphDiv.setGraphBarsAndInputs(cartDataArray);
	},
	/**
	*Builds a brand new Cart Graph Div.
	*
	*@method buildGraphDiv
	*/
	buildGraphDiv:function(){		
		var cl=document.getElementById("cartList");
		var gd=document.createElement("div");//gd=graphDiv
		gd.id="cartGraphDiv";		
		gd.style.position="absolute";
		gd.style.top="0px";
		gd.style.left="0px";
		gd.posX=0;
		gd.posY=0;
		gd.style.width = "500px";
		gd.style.height="200px";
		gd.style.backgroundColor="black";
		exline.CartList.mobilizeDiv(gd);
		exline.CartList.addInteractiveBorders(gd);
		document.body.appendChild(gd);
		exline.cartGraph.addButton(1,"MelloYello",1.5,"$1.50");
		exline.cartGraph.addButton(2,"Doritos",1.1,"$1.10");
		exline.cartGraph.addButton(3,"Twizzlers",2,"$2.00");
		exline.cartGraph.addButton(4,"Snickers",1.35,"$1.35");
		exline.cartGraph.addButton(5,"Ritz Bits",1.25,"$1.25");
		exline.cartGraph.addButton(6,"Doublemint",.75,"$0.75");
		var walletBox = document.createElement("INPUT");
		walletBox.setAttribute("id","cartGraphWalletBox");
		var nameBox = document.createElement("input");
		nameBox.setAttribute("id","cartGraphNameBox");
		var nameLabel = document.createElement('div');
		var walletLabel = document.createElement('div');
		walletBox.style.width="20%";
		nameBox.style.width="20%";
		nameLabel.width="15%";
		walletLabel.width="15%";
		gd.setGraphBarsAndInputs=function(cart){
			gd.cart = cart;			
			walletBox.value=cart.WalletAmt;
			nameBox.value=cart.CartName;
			for(var i = 1; i < 7; i++){
				document.getElementById("btnGroup"+i).quantityLabel.innerHTML=cart['Item'+i+'Amt'];
			}
		};
		nameLabel.style.backgroundColor="lightgrey";
		walletLabel.style.backgroundColor="lightgrey";
		nameLabel.innerHTML="Name of this Cart:";
		walletLabel.innerHTML="Wallet Size:";		
		nameLabel.style.position="absolute"; nameLabel.style.left="1%"; nameLabel.style.padding=".5%"; nameLabel.style.top = "44%";
		nameBox.style.position="absolute"; nameBox.style.top="44%";     nameBox.style.left="26%";
		walletLabel.style.position="absolute"; walletLabel.style.padding=".5%";   walletLabel.style.top="44%"; walletLabel.style.left="60%";
		walletBox.style.position="absolute"; walletBox.style.top = "44%"; walletBox.style.left="77%";
		gd.appendChild(walletBox);
		gd.appendChild(nameBox);
		gd.appendChild(nameLabel);
		gd.appendChild(walletLabel);
		gd.getWalletVal=function(){
			return document.getElementById("cartGraphWalletBox").value;
		}
		gd.getName=function(){
			return document.getElementById("cartGraphNameBox").value;
		}
		gd.coughUpYourData=function(){/*
			alert( this.cart.ID+
				this.cart.CartName+ this.cart.WalletAmt+ this.cart.Item1Amt+ this.cart.Item2Amt+
				this.cart.Item3Amt+ this.cart.Item4Amt+ this.cart.Item5Amt+ this.cart.Item6Amt
			);*/
			return [this.cart.ID,
				this.cart.CartName, this.cart.WalletAmt, this.cart.Item1Amt, this.cart.Item2Amt,
				this.cart.Item3Amt, this.cart.Item4Amt, this.cart.Item5Amt, this.cart.Item6Amt];			
		}
		var deleteBtn = document.createElement("input");
		deleteBtn.type="button";
		deleteBtn.style.position="absolute";
		deleteBtn.style.bottom="5px";
		deleteBtn.style.left="5px";
		deleteBtn.value="Delete this cart";
		deleteBtn.onclick=function(){
			exline.cartUpdater.deactivateCart(document.getElementById("cartGraphDiv").coughUpYourData()[0]);
		};
		gd.appendChild(deleteBtn);
		var notary = document.createElement("div");
		notary.setAttribute("id", "notary"); notary.style.fontSize="23px"; notary.style.color="blue";
		notary.style.position="absolute"; notary.style.left="5px"; notary.style.bottom="35px";
		gd.appendChild(notary);
		var redEx = document.createElement("input");
		redEx.type="button";
		redEx.style.position="absolute";
		redEx.style.right = "5px"; redEx.style.bottom = "5px";
		redEx.value="X"; redEx.style.color="RED"; redEx.style.fontWeight="bold"; //redEx.style.fontSize="20px";
		redEx.onclick=function(){//the next 5 lines of code are exactly from the updateNameAndWallet.onclick function.
			var cartGD = document.getElementById("cartGraphDiv");
			var dat = cartGD.coughUpYourData();
			dat[1]=cartGD.getName();
			dat[2]=cartGD.getWalletVal();			
			exline.cartUpdater.editThisCart(dat, 'close');//close is to close the cart graph.
		};
		gd.appendChild(redEx);
	},
	/**
	*Adds buttons to the Cart Graph Div.  This method is called by the method buildGraphDiv when it runs.
	*
	*@method addButton
	*@param {Number} itemNum Denotes the item number for which the button is associated.
	*@param {String} btnTextName Denotes the name of the associated item.
	*@param {Number} price Denotes the price value of the associated item.
	*@param {String} btnTextPrice Contains the string value of the price of the associated item as it should be seen by the end user.
	*/
	addButton:function(itemNum, btnTextName, price, btnTextPrice){
		var gd=document.getElementById("cartGraphDiv");
		var btnGroup=document.createElement("div");
		btnGroup.setAttribute("id", "btnGroup"+itemNum);
		btnGroup.style.position="absolute";
		btnGroup.style.margin="4px";
		var p = getPosition(itemNum);
		function getPosition(iN){
			if(iN >= 4){
				iN-=3; var yPos=0;
			} else var yPos = 21;
			return[(iN-1)*33, yPos]; 
		}
		btnGroup.style.left = p[0]+"%";
		btnGroup.style.top = p[1]+"%";
		btnGroup.style.backgroundColor="silver";
		btnGroup.style.width="31%";
		btnGroup.style.height="18%";
		btnGroup.style.zIndex=4;		
		btnGroup.innerHTML=btnTextName+"   "+btnTextPrice;
		var upBtn=document.createElement("IMG");
		upBtn.itemNum = itemNum;
		upBtn.style.position="absolute";
		upBtn.style.right="-1%";
		upBtn.style.top="-1%";
		upBtn.src="images/chevronUp.png";
		upBtn.style.width="20%";
		upBtn.style.height="45%";
		upBtn.onclick=function(){
			var cartGD = document.getElementById("cartGraphDiv");
			var dat = cartGD.coughUpYourData();
			dat[2]=cartGD.getWalletVal();//we're updating the wallet every time...
			dat[1]=cartGD.getName();//we're also updating the cart's name every time...
			var res =exline.cartGraph.isCartAccumulationLessMoneyThanWallet('add', dat, itemNum, btnTextName, price);//if there's room in the cart...v
			if(res[0]){	//if the cart was UPDATED in the database...
				dat[this.itemNum+2] = Number(dat[this.itemNum+2])+1;
				exline.cartUpdater.editThisCart(dat, res[1]);
			}else{
				exline.cartGraph.note(res[1]);
			}
		};
		btnGroup.appendChild(upBtn);
		var dwnBtn=document.createElement("IMG");
		dwnBtn.itemNum = itemNum;
		dwnBtn.style.position="absolute";
		dwnBtn.style.right="-1%";
		dwnBtn.style.bottom="-1%";
		dwnBtn.src="images/chevronDown.png";
		dwnBtn.style.width="20%";
		dwnBtn.style.height="45%";
		dwnBtn.onclick=function(){
			var cartGD = document.getElementById("cartGraphDiv");
			var dat = cartGD.coughUpYourData();
			dat[1]=cartGD.getName();//we're updating the wallet every time...
			dat[2]=cartGD.getWalletVal();//we're also updating the cart's name every time...
			if(dat[this.itemNum+2] > 0){
				dat[this.itemNum+2] = dat[this.itemNum+2]-1;			
				exline.cartUpdater.editThisCart(dat);
				exline.cartGraph.note(btnTextName+ "was removed from your cart. Your remaining balance is: $"+
					exline.cartGraph.isCartAccumulationLessMoneyThanWallet('sub', dat, itemNum, btnTextName, price));
			}
		};
		btnGroup.appendChild(dwnBtn);
		btnGroup.quantityLabel=document.createElement("P");
		btnGroup.quantityLabel.style.position="absolute";
		btnGroup.quantityLabel.style.left="0px";
		btnGroup.quantityLabel.style.height="14%";
		btnGroup.quantityLabel.style.bottom="0px";
		btnGroup.appendChild(btnGroup.quantityLabel);
		gd.appendChild(btnGroup);
	},
	/**
	*A complicated method that has multiple uses depending on the parameters passed into it. It returns an array with two components if the first parameter is 'add'. The first of these components is a boolean value, the second is a message String to the user. If the first parameter is 'sub' (short for 'subtract') the method only returns a message.
	*
	*@method isCartAccumulationLessMoneyThanWallet
	*@param {String} addOrSub In usage, the value of the String should either be 'add' or 'sub'. "UP" arrow buttons on the Cart Graph Div send the 'add' parameter.  "DOWN" arrow buttons on the Cart Graph Div send the 'sub' parameter.
	*@param {Array} dat Data from the database in Array format which denotes the ammounts of each of the various items already within the associated Cart object.
	*@param {Number} itemNum Denotes the item that is associated with this specific action.
	*@param {String} itemName Denotes the name of the item that is associated with this specific action.
	*@param {Number} price Denotes the price of the associated item.
	*@return {Array} If @param addOrSub == 'add', an Array is returned.
	*@return {String} If @param addOrSub == 'sub' a String message for the end user is returned.
	*/
	isCartAccumulationLessMoneyThanWallet:function(addOrSub, dat, itemNum, itemName, price){
		dat[2]=Number(dat[2]);
			var remaining=dat[2]- ((dat[3] * 1.5) + (dat[4] * 1.1) + (dat[5]*2) +
			(dat[6]*1.35) + (dat[7]*1.25)+(dat[8]*.75));
		if(addOrSub=='add'){			
			if(remaining-price >=0){
				return[true, itemName+" was successfully added to your cart! You have $"+
				exline.cartGraph.mathFix(remaining-price)+" left in your wallet to spend."];
			}else return[false, itemName+" was not added to your cart. Add funds to your wallet, or remove items from your cart."];
		}else if(addOrSub=='sub'){
			return exline.cartGraph.mathFix(remaining);
		}
	},
	/**
	*Fixes rounding errors brought on my floating point data.
	*
	*@method mathFix
	*@param {Number} num The number which is to be properly rounded.
	*@return {Number} The parameter after being properly rounded.
	*/
	mathFix:function(num){
		return Math.round(1000*num)/1000;   // round num to hundredths
	},
	/**
	*Removes anything that isn't a number from the given parameter except for a decimal, and up to ONE dash symbol (-) only at the Beginning of the String to indicate a negative value. 
	*
	*@method removeSymbology
	*@param String str A String from a user input box which OUGHT to only contain a number.
	*@return {String} Returns the String parameter with non-numeric chars removed except as stated above.
	*/
	removeSymbology:function(str){
		var decimalCount = 0;
		var secondDec = function(x){
			if(x == "."){
				if(decimalCount==0){
					decimalCount = 1;
					return true;
				}else{
					return false;
				}
			} else{
				return false;
			}
		}
		for(var i = 0; i < str.length; i++){
			if(str[i] != "0" && str[i] != "1" && str[i] != "2" && str[i] != "3" && str[i] != "4" && str[i] != "5" && 
			str[i] != "6" && str[i] != "7" && str[i] != "8" && str[i] != "9" && !(str[i] == "-" && i==0) &&
			!secondDec(str[i]) ) {
				if(i==0){
					str=str.slice(1, str.length);  i--;
				}
				else if(i != str.length-1){
					str = str.slice(0, i) + str.slice(i+1, str.length+1); i--;
				}
				else {
					str = str.slice(0, i);
				}
			}
		}
		return str;
	},
	/**
	* Returns the numerical parameter with assurance that there can be only one decimal in the number. Only the first occurance of a decimal will be kept, the rest will be removed.
	*
	*@method removeExcessDecimalPlaces
	*@param {Number} num OR {String} num
	*@return {String} Returns the received param with any decimals but one removed.
	*/
	removeExcessDecimalPlaces:function(num){
		var i = num.indexOf(".");
		var numberOfExcessDecimals = 0;
		if((num.length - i) > 2) numberOfExcessDecimals = num.length-3 - (i);
		if(i==-1)return num;
		else return num.slice(0, (num.length - numberOfExcessDecimals));
	}
}