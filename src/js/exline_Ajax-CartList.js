/**
* @module exline
* @main - This is my exline package/module.
* 
*/
var exline = exline || {};
/**
* This class is the Ajax module.
* @class Ajax
*/
exline.ajax = {
	/**
	*This method sends AJAX requests.
	*
	*@method send This method sends the AJAX requests.
	*@param {Object} options This object contains a series of key/value pairs with the following as keys: method(a literal function to be used as callback), url(The string literal url leading to the Index.php file), isAsynchronous(A boolean value to indicate that this AJAX request should be asynchronous. This should always be true.)
	*/
	send: function(options){
		var http = new XMLHttpRequest();
		http.open(options.method, options.url, options.isAsynchronous);
		if(options.headers){
			for(var key in options.headers){
				//to get the value: someObject[key]
				http.setRequestHeader(key, options.headers[key]);
			}
			//http.setRequestHeader('Origin', 'www.exlinestudios.com');
		}
		http.onreadystatechange = function(){
			if(http.readyState == 4 && http.status == 200){			
				if(options.isAsynchronous){
					if(options.optionalParam) 
						http.addEventListener("load", options.callback(http.responseText, options.optionalParam));
					else http.addEventListener("load", options.callback(http.responseText));
				}else{//if not asynchronous...
					if(options.optionalParam)
						options.callback(http.responseText, options.optionalParam);
					else options.callback(http.responseText);			
				}
			} else if (http.readyState == 4){
				exline.ajax.error(http.status);
			}
		}
		http.send(options.requestBody);
	},
	/**
	* This method is the error handler for my AJAX requests.
	* @method error
	*/
	error: function(errorCode){
		if(errorCode == 2 || (errorCode >= 200 && errorCode < 300)){
			alert("Hurray, your request went through successfully!");
		}else if(errorCode==4 ||(errorCode>=400 && errorCode < 500)){
			var msg = "Something was wrong with the request.";
			if(errorCode==404)msg+=" The file was not found.";
			alert(msg);
		}else if(errorCode==5 ||(errorCode>=500 && errorCode < 600)){
			alert("Something went wrong on the server, the request is not at fault.");
		}else if(errorCode == 404){
			alert("Error Code 404! Your 'get' request failed to find it's recipient.");
		}else if(errorCode == 0){
			alert("The Error Code was 0. You are having a Cross Origin Resource Sharing issue.");
		}else{
			alert("I don't know what happened. - yet.  ErrorCode: "+ errorCode);
		}
	}
};
/**
* This class is the CartList module.
* @class CartList
*/
exline.CartList = {
	/**
	*Indicates if the CartList div as been instantiated.
	*
	*@property hasBeenInitialized
	*@type Boolean
	*@default false
	*/
	hasBeenInitialized : false,
	/**
	*Indicates the X-coordinate the CartList div, since it is dragable on the screen.
	*
	*@property mouseX
	*@type Number
	*@default 0
	*/
	mouseX : 0,
	/**
	*Indicates the Y-coordinate the CartList div, since it is dragable on the screen.
	*
	*@property mouseY
	*@type Number
	*@default 0
	*/
	mouseY : 0,
	/**
	* Indicates which Div is currently being dragged accross the screen, if any.
	*
	*@property divBeingPulled
	*@type Div
	*@default null
	*/
	divBeingPulled:null,
	/**
	* Alerts a message on the screen if there is an AJAX error or a database error.
	*
	* @method errorAlert
	* @param {String} msg The message to be displayed to the user.
	*/
	errorAlert:function(msg){
		var errorBox;
		if(document.getElementById("errorBox")==null)
			exline.CartList.buildErrorBox();
		errorBox=document.getElementById("errorBox");
		errorBox.alert(msg);
	},
	/**
	* Builds the custom error-displaying div to display error messages to the user.
	*
	* @method buildErrorBox
	*/
	buildErrorBox:function(){
		var errorBox = document.createElement("div");
		errorBox.setAttribute("id", "errorBox");
		errorBox.style.width="100%";
		errorBox.style.height="125px";
		errorBox.alert=function(msg){
			if(document.getElementById("errorNotary"))
			document.getElementById("errorNotary").innerHTML=str;
		};
		document.body.appendChild(errorBox);
		var errorNotary = document.createElement("div");
		errorNotary.setAttribute("id", "errorNotary"); errorNotary.style.fontSize="23px"; errorNotary.style.color="blue";
		errorNotary.style.position="absolute"; errorNotary.style.left="5px"; errorNotary.style.bottom="35px";
		errorBox.appendChild(errorNotary);
		var redEx = document.createElement("input");
		redEx.type="button";
		redEx.style.position="absolute";
		redEx.style.right = "5px"; redEx.style.bottom = "5px";
		redEx.value="X"; redEx.style.color="RED"; redEx.style.fontWeight="bold"; //redEx.style.fontSize="20px";
		redEx.onclick=function(){//the next 5 lines of code are exactly from the updateNameAndWallet.onclick function.
			var div = document.getElementById("errorBox");
			div.parentNode.removeChild(div);
		};
		errorBox.appendChild(redEx);
	},
	/**
	* Initializes the module
	* 
	* @method init
	* @param {Array} options The data from the database which contains cart data.
	*/
	init : function(options){		
		exline.CartList.hasBeenInitialized = true;
		options.target.style.height = .8 * (Math.max(document.body.scrollHeight, document.body.offsetHeight, 
                document.documentElement.clientHeight, document.documentElement.scrollHeight,
                document.documentElement.offsetHeight))+"px";
		options.target.style.width = "45%";
		options.target.style.position = "relative";
		options.target.style.backgroundColor = "maroon";//"lightGreen";
		options.target.isClicked=false;
		options.target.posX=0;
		options.target.posY=0;
		options.target.style.zIndex = 0;
		exline.CartList.addInteractiveBorders(options.target);
		exline.CartList.mobilizeDiv(options.target);
		var addNewCartBtn = document.createElement("INPUT");
		addNewCartBtn.setAttribute("id", "addNewCart");
		addNewCartBtn.value = "Create A New Cart";
		addNewCartBtn.type = "button";	
		addNewCartBtn.onclick = exline.cartUpdater.addACart;
		options.target.appendChild(addNewCartBtn);
		document.onkeyup = function(e){
			var activeDiv = document.activeElement;
			exline.cartGraph.note("");
			if(activeDiv.id=="cartGraphWalletBox" || activeDiv.id=="cartGraphNameBox"){
				if(activeDiv.id=="cartGraphWalletBox"){
					activeDiv.value = exline.cartGraph.removeExcessDecimalPlaces(exline.cartGraph.removeSymbology(activeDiv.value));
				}
				//alert(activeDiv.id+" would be updated.");
				var cartGD = document.getElementById("cartGraphDiv");
				var dat = cartGD.coughUpYourData();
				dat[1]=cartGD.getName();
				dat[2]=cartGD.getWalletVal();			
				exline.cartUpdater.editThisCart(dat, false);
			}
		};
	},
	/**
	* Builds the User Interface of the Cart List Div and calls the init method if necessary.
	*
	* @method buildUI
	* @param {Array} options The data from the database which contains cart data.
	*/
	buildUI: function(options){
		if(!exline.CartList.hasBeenInitialized){
			exline.CartList.init(options);
		}
		var cartCorral = document.createElement("DIV");						
		cartCorral.style.zIndex = 1;
		cartCorral.style.width = "50px";
		cartCorral.style.height = "50px";
		cartCorral.style.backgroundColor = "white";
		cartCorral.style.position = "absolute";		
		cartCorral.style.top = "20%";
		cartCorral.style.left = "2.5%";
		cartCorral.style.width = "95%";
		cartCorral.style.height = "75%";
		document.getElementById("cartList").appendChild(cartCorral);
		for(var i = 0; i < options.data.length; i++){
			var cart = document.createElement("IMG");
			cart.setAttribute("id", "cart"+options.data[i]['ID']);
			cart.ID = options.data[i]['ID'];
			cart.data=[];
			cart.setData = function(dat){
				cart.data['ID'] = dat['ID'];
				cart.data['CartName'] = dat['CartName'];
				cart.data['WalletAmt']= dat['WalletAmt'];
				cart.data['Item1Amt'] = dat['Item1Amt'];
				cart.data['Item2Amt'] = dat['Item2Amt'];
				cart.data['Item3Amt'] = dat['Item3Amt'];
				cart.data['Item4Amt'] = dat['Item4Amt'];
				cart.data['Item5Amt'] = dat['Item5Amt'];
				cart.data['Item6Amt'] = dat['Item6Amt'];
			};
			cart.setData(options.data[i]);
			cart.onclick=function(){exline.cartGraph.graphThisCart(this.data);};
			cart.style.display="inline-block";
			cart.src = "images/shopping-cart-icon-515.png";
			cart.style.width = "90px";//"12.5%";
			cart.style.height = "90px";//"12.5%";
			cartCorral.appendChild(cart);			
		}		
	},
	/**
	* Adds a silver border to the right side of the DIV being passed in
	*
	* @method addInteractiveBorders
	* @param {DIV} div
	*/
	addInteractiveBorders:function(div){
		//div.iWidth=div.style.width.substring(0, div.style.width.length-2);
		//div.iHeight=div.style.height.substring(0, div.style.height.length-2);
		var rightBorder = document.createElement("div");
		rightBorder.style.zIndex="3";
		rightBorder.style.position="absolute";
		rightBorder.style.right="-5px";
		rightBorder.style.backgroundColor="silver";
		rightBorder.style.width="5px";
		rightBorder.style.height=div.style.height;
		div.appendChild(rightBorder);
	},
	/**
	* Adds functionality to the passed in DIV which allows it to be mouse-dragged accross the screen by the user.
	*
	* @method mobilizeDiv
	* @param {DIV} div
	*/
	mobilizeDiv:function(div){
		div.onmousedown = function(){
			div.isClicked = true;
			exline.CartList.divBeingPulled = div;
			//div.style.backgroundColor = "lightBlue";
		};
		div.onmouseup=function(){
			div.isClicked = false;
			exline.CartList.divBeingPulled = null;
			//div.style.backgroundColor = "red";
		};
		div.onmouseout=function(){
			if(div.isClicked){
				div.onmouseup();
			}
			div.isClicked = false;
		};
		document.onmousemove=function(m){						
			if(exline.CartList.divBeingPulled){
				var dbp = exline.CartList.divBeingPulled;
				var mouseDiffX = exline.CartList.mouseX - m.clientX;		
				var mouseDiffY = exline.CartList.mouseY - m.clientY;
				//console.log("clientY: "+ m.clientY +",  clientX: "+m.clientX);
				dbp.style.left = (dbp.posX - mouseDiffX)+"px";
				dbp.style.top  = (dbp.posY - mouseDiffY)+"px";
				dbp.posX = (dbp.posX - mouseDiffX);
				dbp.posY  = (dbp.posY - mouseDiffY);
			}
			exline.CartList.mouseX = m.clientX;		
			exline.CartList.mouseY = m.clientY;	
		};
	},
	/**
	* Retreives data from the database. The callback function builds the calls the buildUI method with the retreived data as the parameter.
	*
	* @method getAllCarts
	*/
	getAllCarts:function(){
		exline.ajax.send({
			errorCallback: function(statusCode){
				alert("ERROR: "+ statusCode);
			},
			callback: buildPage,
			isAsynchronous : true,
			headers:{"Content-Type":"text/plain"},
			//local//url: "http://localhost/Fall/adv-topics/adv-topics-final/web-service/carts/",
			url: "http://www.exlinestudios.com/blog/web-development/AdvTopicsFinal/web-service/carts/",
			method: "GET"
		});
		function buildPage(response){
			var carts = JSON.parse(response);
			var div = document.getElementById("cartList");
			exline.CartList.buildUI({target:div, data:carts});
		}
	}
};
window.addEventListener('load', function(){
	exline.CartList.getAllCarts();
});