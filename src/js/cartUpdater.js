var exline = exline || {};
/**
* This class is the CartUpdater module, which uses the ajax class to send ajax requests..
* @class CartUpdater
*/
exline.cartUpdater = {	
	/**
	* This method sends errors for the cartUpdater class. Creates a custom alert event that an error occurs.
	* @method error
	* @param {Number} errorCode
	*/
	error: function(errorCode){
		if(errorCode == 2 || (errorCode >= 200 && errorCode < 300)){
			exline.CartList.errorAlert("Hurray, your request went through successfully!");
		}else if(errorCode==4 ||(errorCode>=400 && errorCode < 500)){
			exline.CartList.errorAlert("Something was wrong with the request.");
		}else if(errorCode==5 ||(errorCode>=500 && errorCode < 600)){
			exline.CartList.errorAlert("Something went wrong on the server, the request is not at fault.");
		}
	},
	/**
	* This method sends an AJAX request so that the database will create a brand new empty cart.
	* @method addACart
	*/
	addACart:function(){
		exline.ajax.send({
			errorCallback: exline.cartUpdater.alertTheResponse,
			callback: exline.CartList.getAllCarts,
			isAsynchronous : true,
			//headers:{"Content-Type":"application/json"},
			url: "http://www.exlinestudios.com/blog/web-development/AdvTopicsFinal/web-service/addCart/",
			//local//url: "http://localhost/Fall/adv-topics/adv-topics-final/web-service/addCart/",
			method: "POST"	
		});
	},
	/**
	* This method uses the AJAX class to update carts in the database.
	* @method editThisCart
	* @param {Array} dat This is an array 	
	* @param {String} optional This optional parameter defaults to false but, if used, is intended to take a String.  This parameter can be used to close the Cart Graph Div and to alert messages to the user.
	*/
	editThisCart:function(dat, optional=false){
		try{
			exline.ajax.send({
				requestBody: '{"ID":'+dat[0]+', "CartName":"'+dat[1]+'", "WalletAmt":"'+dat[2]+
				'", "Item1Amt":"'+dat[3]+'", "Item2Amt":"'+dat[4]+'", "Item3Amt":"'+dat[5]+'", "Item4Amt":"'+dat[6]+
				'", "Item5Amt":"'+dat[7]+'", "Item6Amt":"'+dat[8]+'"}',
				errorCallback: exline.cartUpdater.alertTheResponse,
				optionalParam: optional, 
				callback: exline.cartGraph.refreshCart,
				isAsynchronous : true,
				//headers:{"Content-Type":"application/json"},
				url: "http://www.exlinestudios.com/blog/web-development/AdvTopicsFinal/web-service/carts/"+dat[0],
				//local//url: "http://localhost/Fall/adv-topics/adv-topics-final/web-service/carts/"+dat[0],
				method: "PUT"
			});
		}catch(err){
			alert("Are you trying to hack my data-base?");
		}
	},
	/**
	* This method uses the AJAX class to update a cart in the database, setting it's 'Active' value from true to false, which is a "virtual delete".
	* @method deactivateCart
	* @param {Number} id  The unique key of the cart in the database to be "deactivated".
	*/
	deactivateCart:function(id){
		exline.ajax.send({
			requestBody: id,
			method: "PUT",
			url: "http://www.exlinestudios.com/blog/web-development/AdvTopicsFinal/web-service/deactivate",
			//local//url: "http://localhost/Fall/adv-topics/adv-topics-final/web-service/deactivate",
			callback: exline.cartUpdater.refreshCartList,
			isAsynchronous:true
		});		
		exline.cartGraph.closeCartGraph();
	},
	/**
	* This method uses the AJAX class to get all data from all carts from the database. This method has a callback which uses that data to build the cartList div using the data.
	* @method refreshCartList
	*/
	refreshCartList:function(input){
		try{
			exline.ajax.send({
				//requestBody: "This is the body of the request",//If method is GET or DELETE then Body will be NULL
				errorCallback: this.errorCode,
				callback: exline.cartUpdater.setUpCartList,
				isAsynchronous : true,
				//headers:{"Content-Type":"text/plain"},
				url: "http://www.exlinestudios.com/blog/web-development/AdvTopicsFinal/web-service/carts/",  //"carts" is the GET request; this will point to index.php because that is the default auto to go to if not specified.
				//url: "http://localhost/Fall/adv-topics/adv-topics-final/web-service/carts/",  //"carts" is the GET request; this will point to index.php because that is the default auto to go to if not specified.
				method: "GET"
			});
		}catch(err){
			alert("Are you trying to hack my module?");
		}
	},
	/**
	* This method simply alerts a string.  It is meant to be used as a debugger tool.  Simply use 'alertTheResponse' as a callback in one of the other methods in this class to see what you're getting back from the database. Not for end-user use!
	* @method alertTheResponse
	* @param {String} r  This message to be alerted.  Intended to be the response from an Ajax request.
	*/
	alertTheResponse:function(r){
		alert(r);
	},
	/**
	* This method is designed to be used as a call-back for other methods in this class. It takes an AJAX JSON data array response from the database re-builds the cartList UI by calling a method from the CartList class and passing the data into it.
	* @method setUpCartList
	* @param response  A JSON response from the database, passed in by one of the other methods in this class that use the AJAX class.
	*/ 
	setUpCartList:function(response){
		//alert("This is the array that should be printed on the screen as an unordered list: " + response);
		var carts = JSON.parse(response);
		var div = document.getElementById("cartList");
		var cartList = exline.CartList.buildUI({target:div, data:carts});		
	}
}
