<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true ");
header("Access-Control-Allow-Methods: OPTIONS, GET, POST, PUT");
header("Access-Control-Allow-Headers: Content-Type, content-type, Depth, User-Agent, X-File-Size, 
    X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
?>
<?php
define("DEBUG_MODE", true);
define("DB_HOST", "localhost");
define("DB_NAME", "exlinest_cartDB");
define("DB_USER", "exlinest_Matthew");
define("DB_PASSWORD", "989studios");
$link = get_link();
function get_link(){
  global $link;   
  if($link == null){
      $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  }
  return $link;
}
require("CartDataAccess.php");
$da = new CartDataAccess($link);
$url_path = isset($_GET['url_path']) ? $_GET['url_path'] : "";
$method = $_SERVER['REQUEST_METHOD'];
$request_body = file_get_contents('php://input');
$request_headers = getallheaders();
//if($method!="PUT"){};else die($_SERVER['REQUEST_METHOD']);
if($method == "GET" && empty($url_path)){
  die("This shouldn't have happened.");
 	//Show the home page for this web service
  //require("api-docs.php");
}else if($method == "GET" && ($url_path == "carts/" || $url_path == "carts")){
  get_all_carts();
}else if($method == "POST" && ($url_path == "addCart/" || $url_path == "addCart")){  	
  insert_cart();
}else if($method=="PUT" && $url_path=="deactivate"){
  deactivateCart($request_body);
}else if($method == "PUT" ){// && preg_match('/^carts\/([0-9]*\/?)$/', $url_path, $matches)){
  // Get the id of the cart from the regular expression
  //$cart_id = $matches[1]; // we can get the cart id from the request body
  //die($matches[1]);
  update_cart();
}else{
  //header('HTTP/1.1 404 Not Found', true, 404);
  die("We're sorry, we can't find this page: {$_SERVER['REQUEST_METHOD']} {$_SERVER['REQUEST_URI']}");
}
/////////////////////////////////
// FUNCTIONS
/////////////////////////////////
function get_all_carts(){
  global $da, $url_path, $method, $request_body, $request_headers;  
  $carts = $da->get_all_carts();
  if(!$carts){
    header('HTTP/1.1 500 server error (fetching carts from the database)', true, 500);
    die();
  }
  $return_format = $request_headers['Accept'];
  //header("Content-Type","application/json");
  $json = json_encode($carts);
  echo($json);
  die();
}

function deactivateCart($id){
  global $da;
  $da->deactivateCart($id);
}

function insert_cart(){
  global $da;
  $da->insert_cart();
}

function update_cart(){
  global $da, $url_path, $method, $request_body, $request_headers;  
  if($cart = json_decode($request_body, true)){
    //echo(implode(",",$cart));
    $cart = $da->update_cart($cart);
    if($cart){
      //header("Content-Type","application/json");
      die(json_encode($cart));
    }else{
      header('HTTP/1.1 500 - Unable to update cart in the database', true, 500);
      die();
    }
  }else{
    header('HTTP/1.1 400 - Invalid cart data in the request body', true, 400);
    die();
  }
}
?>