<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true ");
header("Access-Control-Allow-Methods: OPTIONS, GET, POST, PUT");
header("Access-Control-Allow-Headers: Content-Type, content-type, Depth, User-Agent, X-File-Size, 
    X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
?>
<?php
class CartDataAccess{	
	private $link;
	/**
	* Constructor
	* @param $link 		The connection to the database
	*/
	function __construct($link){
		$this->link = $link;
	}
	/**
	* Get all carts
	* @param $order_by 		Optional - the sort order (title or author)
	* @return 2d array 		Returns an array of carts (each cart is an assoc array)
	*/
	function get_all_carts(){
		$qStr = "SELECT	ID, CartName, WalletAmt, Item1Amt, Item2Amt, Item3Amt, Item4Amt, Item5Amt, Item6Amt FROM carts WHERE IsActive = true";
		$result=mysqli_query($this->link, $qStr) or die("$result");
		//die($result);
		$all_carts = array();
		while($row = mysqli_fetch_assoc($result)){
			$cart = array();
			$cart['ID'] = htmlentities($row['ID']);
			$cart['CartName'] = htmlentities($row['CartName']);
			$cart['WalletAmt'] = htmlentities($row['WalletAmt']);
			$cart['Item1Amt'] = htmlentities($row['Item1Amt']);
			$cart['Item2Amt'] = htmlentities($row['Item2Amt']);
			$cart['Item3Amt'] = htmlentities($row['Item3Amt']);
			$cart['Item4Amt'] = htmlentities($row['Item4Amt']);
			$cart['Item5Amt'] = htmlentities($row['Item5Amt']);
			$cart['Item6Amt'] = htmlentities($row['Item6Amt']);
			$all_carts[] = $cart;
		}
		if($all_carts){
			return $all_carts;
		} else{
			return ["There are no carts in the database."];
		}
	}
	/**
	* Get a cart by its ID
	* @param $id 			The ID of the cart to get
	* @return array 		An assoc array that has keys for each property of the cart
	*/
	function deactivateCart($id){		
		$qStr = "UPDATE carts SET IsActive= false WHERE ID = " . $id;
		//die($qStr);
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));	
	}
	/**
	* Inserts a fresh, empty cart into the database.
	*/
	function insert_cart(){
		$qStr = "INSERT INTO carts (
					CartName, WalletAmt, Item1Amt, Item2Amt,
					Item3Amt, Item4Amt, Item5Amt, Item6Amt, IsActive
				) VALUES (
					'New Cart!', '0', '0', '0', '0', '0', '0', '0', '1'
				)";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		if($result){
			// add the cart id that was assigned by the data base
			$cart['ID'] = mysqli_insert_id($this->link);
			return $cart;
		}else{
			$this->handle_error("unable to insert cart");
		}
		return false;
	}
	/**
	* Updates a row in the database.
	* @param $cart 			A cart object as an array.
	* @return array 		Returns the cart object if the update is successful, or returns false.
	*/
	function update_cart($cart){
		// prevent SQL injection
		$cart['ID'] = mysqli_real_escape_string($this->link, $cart['ID']);
		$cart['CartName'] = mysqli_real_escape_string($this->link, $cart['CartName']);
		$cart['WalletAmt'] = mysqli_real_escape_string($this->link, $cart['WalletAmt']);
		$cart['Item1Amt'] = mysqli_real_escape_string($this->link, $cart['Item1Amt']);
		$cart['Item2Amt'] = mysqli_real_escape_string($this->link, $cart['Item2Amt']);
		$cart['Item3Amt'] = mysqli_real_escape_string($this->link, $cart['Item3Amt']);
		$cart['Item4Amt'] = mysqli_real_escape_string($this->link, $cart['Item4Amt']);
		$cart['Item5Amt'] = mysqli_real_escape_string($this->link, $cart['Item5Amt']);
		$cart['Item6Amt'] = mysqli_real_escape_string($this->link, $cart['Item6Amt']);

		$qStr = "UPDATE carts SET CartName='".$cart['CartName']."', WalletAmt='".$cart['WalletAmt']."', Item1Amt='".$cart['Item1Amt']."', Item2Amt='".$cart['Item2Amt'].
		"', Item3Amt='".$cart['Item3Amt']."', Item4Amt='".$cart['Item4Amt']."', Item5Amt='".
		$cart['Item5Amt']."', Item6Amt='".$cart['Item6Amt']."' WHERE ID = " . $cart['ID'];
		//die($qStr);
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		if($result){
			return $cart;
		}else{
			$this->handle_error("unable to update cart");
		}
		return false;
	}
	/**
	* Returns the error.
	* @param $e			The error.
	* @return $e 		The error.
	*/
	function handle_error($e){
		return $e;
	}
}